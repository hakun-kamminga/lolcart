SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `locart_category`;
CREATE TABLE `locart_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(200) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `locart_category` (`category_id`, `category_name`) VALUES
(1,	'Cardboard boxes - S/M/L');

DROP TABLE IF EXISTS `lolcart_currency`;
CREATE TABLE `lolcart_currency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` char(3) NOT NULL,
  `title` varchar(60) NOT NULL,
  `Symbol` char(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lolcart_media`;
CREATE TABLE `lolcart_media` (
  `media_id` int(11) NOT NULL AUTO_INCREMENT,
  `media_product_id` int(11) NOT NULL,
  `media_uri` varchar(255) NOT NULL,
  `media_created` datetime NOT NULL,
  `media_updated` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `lolcart_product`;
CREATE TABLE `lolcart_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_category_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_description` text NOT NULL,
  `product_code` char(10) NOT NULL,
  `product_price` decimal(10,6) NOT NULL DEFAULT '0.000000',
  `product_has_weight` tinyint(1) NOT NULL DEFAULT '1',
  `product_weight` decimal(10,3) NOT NULL DEFAULT '0.000',
  `product_created` datetime NOT NULL,
  `product_updated` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `lolcart_product` (`product_id`, `product_category_id`, `product_name`, `product_description`, `product_code`, `product_price`, `product_has_weight`, `product_weight`, `product_created`, `product_updated`) VALUES
(1,	1,	'Small Box',	'2.4 x 4.8 cm',	'BOX_SM',	2.990000,	1,	0.000,	'2020-03-29 15:45:45',	'2020-03-30 21:21:29'),
(2,	1,	'Medium Box',	'4.8 x 9.6 cm',	'BOX_MD',	4.990000,	1,	0.000,	'2020-03-29 15:47:20',	'2020-03-30 21:21:24'),
(3,	1,	'Large Box',	'20 x 20 cm',	'BOX_LG',	7.990000,	1,	0.000,	'2020-03-30 21:21:17',	'2020-03-30 21:21:17');

DROP TABLE IF EXISTS `lolcart_settings`;
CREATE TABLE `lolcart_settings` (
  `setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `setting_name` varchar(100) NOT NULL,
  `setting_value` int(11) NOT NULL,
  `setting_table` varchar(100) NOT NULL,
  PRIMARY KEY (`setting_id`),
  KEY `setting_value` (`setting_value`),
  CONSTRAINT `lolcart_settings_ibfk_1` FOREIGN KEY (`setting_value`) REFERENCES `settings` (`setting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 2020-03-31 10:56:12
