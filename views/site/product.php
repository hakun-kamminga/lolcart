<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Catalogue';
$this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'][] = $product[0]['product_name'];
?>
<div class="site-about">
    <h1><?= Html::encode($product[0]['product_name']) ?></h1>
	<h2>Features</h2>
<?php


if (!empty($product)) {
	
	?>
	<p>Code:  <?= $product[0]['product_code'] ?></p>
	<p>Price: <?= round($product[0]['product_price'],2) ?></p>
	<p>Information: <?= nl2br($product[0]['product_description']) ?></p>
	<?php
}

?>
<button class="btn btn-primary">Add to cart</button>
</div>
