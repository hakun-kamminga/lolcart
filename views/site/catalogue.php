<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Choose your size';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Find your favourite products here!
    </p>
    <div class="row">
    	<div class="col col-lg-6">
    		<?php

			
			if (!empty($products)) {
				
				foreach($products as $product) {
					
					$controls = '<button onclick="qtyChange(-1, ' . $product['product_id'] . ', this)" class="btn btn-default">-</button> <input style="text-align:center;width:40px" value="0" id="qty_' . $product['product_id'] . '" type="text"> 
					<button onclick="qtyChange(1, ' . $product['product_id'] . ', this)" class="btn btn-default">+</button>';
					
					echo '<div class="alert alert-primary"><h3>' . 
					Html::encode($product['product_name']) . ' (£' . round($product['product_price'], 2) . ')';
					
					echo '<span class="pull-right">' . $controls . '</span></h3></div>';
				}
			}

?>
    	

</div>
    	<div class="col col-lg-6" id="summary">
    		<!--
	    	Summary in here
	    	-->
    	</div>
</div>
<button class="btn btn-success">Checkout</button>
</div>
<script>

	function qtyChange(qty,product) {
		
		$.get('/lolcart/basic/web/cart?product_id=' + product + '&add=' + qty, function(response){	// Should make this sync later to prevent double click circus
			updateSummary(response, product);
		});
	}
	function updateSummary(cart, id) {
		var html  = '<ul class="list-group">';
		var keys  = Object.values(cart);
		var total = 0;
		
		for(var i=0; i<keys.length; i++) {
			
			html += '<li class="list-group-item active"><strong>Product:</strong> ' + keys[i].data.product_name + '</li>';
			html += '<li class="list-group-item text-right"><strong>Quantity:</strong> ' + keys[i].qty + '</li>';
			html += '<li class="list-group-item text-right"><strong>Price:</strong> £' + parseFloat(keys[i].data.product_price).toFixed(2) + '</li>';
			html += '<li class="list-group-item text-right"><strong>Sub-Total:</strong> ' + (parseFloat(keys[i].data.product_price) * parseInt(keys[i].qty)).toFixed(2) + '</li>';
			
			total += (parseFloat(keys[i].data.product_price) * parseInt(keys[i].qty));
			
			$('#qty_' + keys[i].data.product_id).val(keys[i].qty);
		}
		html += '<li class="list-group-item text-right"><strong>Total:</strong> ' + total.toFixed(2) + '</li>';
		html += '</ul>';
		$('#summary').html(html);
	}
</script>