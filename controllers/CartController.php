<?php
namespace app\controllers;

use yii\rest\ActiveController;
use app\models\LolcartProduct;

class CartController extends \yii\rest\Controller
{
    public $modelClass = 'app\models\LolcartCart';
    public function init()
    {
    	//unset($_SESSION['cart']);
    	if (null !== (\Yii::$app->request->get('add'))) {
    		
    		$this->addToCart(
    			\Yii::$app->request->get('product_id'),
    			empty(\Yii::$app->request->get('add')) ? 1 : \Yii::$app->request->get('add')
    		);
    	}
    	
    	if (null !== (\Yii::$app->request->get('remove'))) {
    		
    		$this->removeProduct(
    			\Yii::$app->request->get('product_id')
    		);
    	}
    	header('Content-Type: application/json');
    	
    	echo json_encode($_SESSION['cart']);
    	exit();
    }
    private function addToCart(int $productId, int $qty)
    {
    	$_SESSION['cart'][$productId] = [
    		'qty'  => !isset($_SESSION['cart'][$productId]) ? $qty : $_SESSION['cart'][$productId]['qty'] + $qty,
    		'data' => $this->getProduct($productId)
    	]; 
    	
    	if ($_SESSION['cart'][$productId]['qty'] <= 0) {	// Move useless data to GC
    		$this->removeProduct($productId);
    	}
    	return;
    }
    private function getProduct(int $productId)
    {
    	return LolcartProduct::find()->where(['product_id' => $productId])->one()->toArray();
    }
    private function removeProduct(int $productId)
    {
    	unset($_SESSION['cart'][$productId]);
    }
}