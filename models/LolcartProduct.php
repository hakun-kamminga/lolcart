<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lolcart_product".
 *
 * @property int $product_id
 * @property string $product_name
 * @property string $product_description
 * @property string $product_code
 * @property float $product_price
 * @property int $product_has_weight
 * @property float $product_weight
 * @property string $product_created
 * @property string $product_updated
 */
class LolcartProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lolcart_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_name', 'product_description', 'product_code', 'product_created', 'product_updated'], 'required'],
            [['product_description'], 'string'],
            [['product_price', 'product_weight'], 'number'],
            [['product_has_weight'], 'integer'],
            [['product_created', 'product_updated'], 'safe'],
            [['product_name'], 'string', 'max' => 100],
            [['product_code'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'product_category_id' => 'Product Category ID',
            'product_name' => 'Product Name',
            'product_description' => 'Product Description',
            'product_code' => 'Product Code',
            'product_price' => 'Product Price',
            'product_has_weight' => 'Product Has Weight',
            'product_weight' => 'Product Weight',
            'product_created' => 'Product Created',
            'product_updated' => 'Product Updated',
        ];
    }

    /**
     * {@inheritdoc}
     * @return LolcartProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LolcartProductQuery(get_called_class());
    }
}
