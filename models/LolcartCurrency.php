<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lolcart_currency".
 *
 * @property int $id
 * @property string $iso
 * @property string $title
 * @property string $Symbol
 */
class LolcartCurrency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lolcart_currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iso', 'title', 'Symbol'], 'required'],
            [['iso'], 'string', 'max' => 3],
            [['title'], 'string', 'max' => 60],
            [['Symbol'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'iso' => 'Iso',
            'title' => 'Title',
            'Symbol' => 'Symbol',
        ];
    }

    /**
     * {@inheritdoc}
     * @return LolcartCurrencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LolcartCurrencyQuery(get_called_class());
    }
}
