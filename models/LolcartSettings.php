<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lolcart_settings".
 *
 * @property int $setting_id
 * @property string $setting_name
 * @property int $setting_value
 * @property string $setting_table
 *
 * @property Settings $settingValue
 */
class LolcartSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lolcart_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['setting_name', 'setting_value', 'setting_table'], 'required'],
            [['setting_value'], 'integer'],
            [['setting_name', 'setting_table'], 'string', 'max' => 100],
            [['setting_value'], 'exist', 'skipOnError' => true, 'targetClass' => Settings::className(), 'targetAttribute' => ['setting_value' => 'setting_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'setting_id' => 'Setting ID',
            'setting_name' => 'Setting Name',
            'setting_value' => 'Setting Value',
            'setting_table' => 'Setting Table',
        ];
    }

    /**
     * Gets query for [[SettingValue]].
     *
     * @return \yii\db\ActiveQuery|SettingsQuery
     */
    public function getSettingValue()
    {
        return $this->hasOne(Settings::className(), ['setting_id' => 'setting_value']);
    }

    /**
     * {@inheritdoc}
     * @return LolcartSettingsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LolcartSettingsQuery(get_called_class());
    }
}
